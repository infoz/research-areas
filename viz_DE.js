// -- customize according to your needs --------------
const fileName = "dataDE.csv";
const maxNrofFields = 20;

const maxWidth = 643;
const maxHeight = 1250;
const margin = { top: 20, right: 20, bottom: 20, left: 20 };
const profNameWidth = 170;
const spacingV = 20;
const spacingH = 200;
const instituteLegendSpacer = 4; // Spacer that seperates institute list from upper content

const instituteLinks = {
    "ICB": "https://www.chab.ethz.ch/forschung/institute-und-laboratorien/ICB.html",
    "IPW": "https://www.chab.ethz.ch/forschung/institute-und-laboratorien/IPW.html",
    "LAC": "https://www.chab.ethz.ch/forschung/institute-und-laboratorien/LAC.html",
    "LOC": "https://www.chab.ethz.ch/forschung/institute-und-laboratorien/LOC.html",
    "LPC": "https://www.chab.ethz.ch/forschung/institute-und-laboratorien/LPC.html"
}

const instituteColors = {
    "ICB": "rgb(130, 146, 85)",
    "IPW": "rgb(13, 61, 86)",
    "LAC": "rgb(241, 108, 32)",
    "LOC": "rgb(205, 88, 73)",
    "LPC": "rgb(213, 182, 61)"
}
// -------------------------------------------------

const width = maxWidth - margin.left - margin.right;
const height = maxHeight - margin.top - margin.bottom;
let highlightedProf = undefined;


// Create base SVG
const svg = d3.select("#viz")
   .append("div")
   .classed("svg-container", true)
   .append("svg")
   .attr("preserveAspectRatio", "xMinYMin meet")
   .attr("viewBox", "0 0 " + width + " " + height)
   .classed("svg-content-responsive", true)
   .append('g')
   .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');    


// Callback when CSV is loaded
d3.csv(fileName, (error, data) => {
    
    // Error handling
    if (error) {
        const errorText = 'Unable to find or properly parse file "' + fileName + '" for interactive visualization.';
        document.getElementById("viz").innerHTML = errorText;
        console.log(errorText);
        return;
    }


    // Gather field names
    let fields = [];
    data.forEach(d => {
        for (let i=1; i<=maxNrofFields; i++) {
            d["Field"+i] = (d["Field"+i] == undefined) ? "" : d["Field"+i];
            fields.push(d["Field"+i]);
        }
    });
    fields = [...(new Set(fields))]
        .filter(field => field !== '')
        .sort();  // remove duplicates and empty strings, and sort alphabetically


    // Gather coordinates for headers:
    const coords = {};
    coords["RightHeader"] = {
        x: profNameWidth,
        y: 0
    };
    coords["LeftHeader"] = {
        x: profNameWidth+spacingH,
        y: 0
    };


    // Gather coordinates for nodes and edges:
    data.forEach( (d, i) => {
        coords[d["Professor"]] = {
            x: profNameWidth,
            y: spacingV*(i+2)
        }

    });
    fields.forEach( (f,j) => {
        coords[f] = {
            x: profNameWidth+spacingH,
            y: spacingV*(j+2)            
        }

    });


    // Gather coordinates for legend:
    const nrOfProfs = data.length;
    const nrOfFields = fields.length;
    Object.keys(instituteColors)
        .forEach((inst, k) => {
            coords[inst] = {
                x: nrOfProfs>nrOfFields ? profNameWidth+spacingH : profNameWidth,
                y: nrOfProfs>nrOfFields ? spacingV*(nrOfFields+instituteLegendSpacer+k) : spacingV*(nrOfProfs+instituteLegendSpacer+k)
            };
        });


    // Place headers:
    svg
        .append('g')
        .attr('transform', 'translate(' + coords["RightHeader"].x + ',' + coords["RightHeader"].y + ')')
        .append('text')
        .attr('class', 'header')
        .text('Research Group')
        .attr("text-anchor", "end")
        .attr('transform', d => 'translate(' + (-5) + ',' + 5 + ')')
    svg
        .append('g')        
        .attr('transform', 'translate(' + coords["LeftHeader"].x + ',' + coords["LeftHeader"].y + ')')
        .append('text')
        .attr('class', 'header')
        .text('Research Area')
        .attr('transform', d => 'translate(' + 5 + ',' + 5 + ')');        


    // Gather links with their start- and end-coordinates
    const links = [];
    data.forEach(d => {
        for (let i=1; i<=maxNrofFields; i++) {
            const field = d["Field"+i];
            if (field !== "") {
                links.push({
                    prof: d["Professor"],
                    field: field,
                    start: coords[d["Professor"]],
                    end: coords[field],                    
                });
            }
        }
    });    


    // Add each professor node 
    const profNodes = svg.selectAll(".node-prof")
        .data(data)
        .enter()
        .append('g')
        .attr('transform', d => 'translate(' + coords[d["Professor"]].x + ',' + coords[d["Professor"]].y + ')')
        .append('text')
        .attr('class', 'node-prof')
        .on("mouseover", mouseOverProf)
        .on("click", clickOnProf)
        .attr('transform', d => 'translate(' + (-5) + ',' + 5 + ')')
        .attr("text-anchor", "end")
        .text(d => d["Professor"])
        .style("fill", d => instituteColors[d["Institute"]]);

    function mouseOverProf(d) {
        profNodes.classed("node-prof-highlight", false);
        profNodes.classed("node-prof-highlight-linked", false);
        fieldNodes.classed("node-field-highlight", false);
        edges.classed("link-highlight", false);
        profNodes.classed("node-prof-highlight-linked", p => p.Professor === d.Professor);
        edges.filter(e => e.prof === d.Professor)
            .classed("link-highlight", true)
            .raise();
        fieldNodes.filter(f => {
            for (let i=1; i<=maxNrofFields; i++) {
                const field = d["Field"+i];
                if (field === f) {
                    return true;
                }
            }
            return false;
        }).classed("node-field-highlight", true);
        setTimeout(() => highlightedProf = d.Professor, 100); // hack for Android mobile devices
            
    }

    function clickOnProf(d) {
        if (d.Professor != highlightedProf) {
            return;
        }
        const win = window.open(d.Homepage, '_blank');
        win.focus();
    }

        
    // Add each field node 
    const fieldNodes = svg.selectAll(".node-field")
        .data(fields)
        .enter()
        .append('g')
        .attr('class', 'node-field')
        .attr('transform', d => 'translate(' + coords[d].x + ',' + coords[d].y + ')')
        .append('text')
        .on("mouseover", mouseOverField)
        .attr('transform', d => 'translate(' + 5 + ',' + 5 + ')')
        .attr("text-anchor", "start")
        .text(d => d);

    function mouseOverField(d) {
        highlightedProf = undefined;
        profNodes.classed("node-prof-highlight", false);
        profNodes.classed("node-prof-highlight-linked", false);
        fieldNodes.classed("node-field-highlight", false);
        edges.classed("link-highlight", false);
        fieldNodes.filter(f => f === d).classed("node-field-highlight", true);   
        edges.filter(e => e.field === d)
            .classed("link-highlight", true)
            .raise();     
        profNodes.filter(p => {
            for (let i=1; i<=maxNrofFields; i++) {
                const field = p["Field"+i];
                if (field === d) {
                    return true;
                }
            }
            return false;
        }).classed("node-prof-highlight", true);
    }        


    // Add each edge
    const edges = svg.selectAll(".link")
        .data(links)
        .enter()
        .append("path")
        .attr("class", "link")
        .attr("d", d => "M" + d.start.x + "," + d.start.y 
                            + "C" + (d.start.x+d.end.x)/2 + "," + d.start.y 
                            + " " + (d.start.x+d.end.x)/2 + "," + d.end.y 
                            + " " + d.end.x + "," + d.end.y ); 


    // Add legend words
    const legendItems = svg.selectAll(".legend")
        .data(Object.keys(instituteColors))
        .enter()
        .append('g')
        .attr('class', 'legend')
        .attr('transform', d => 'translate(' + coords[d].x + ',' + coords[d].y + ')');
        
    legendItems
        .append('circle')
        .attr('transform', d => nrOfProfs>nrOfFields ? 'translate(' + 10 + ',' + 0 + ')' : 'translate(' + (-10) + ',' + 0 + ')')
        .attr('r', 5)
        .attr("fill", d => instituteColors[d]);

    legendItems
        .append('text')
        .attr('transform', d => nrOfProfs>nrOfFields ? 'translate(' + 20 + ',' + 5 + ')' : 'translate(' + (-20) + ',' + 5 + ')')
        .attr("text-anchor", d => nrOfProfs>nrOfFields ? "start" : "end")
        .attr('class', 'institute')
        .text(d => d)
        .on("click", d => clickOnInstitute(d))
        .style("fill", d => instituteColors[d]);

    function clickOnInstitute(d) {
        const win = window.open(instituteLinks[d], '_blank');
        win.focus();        
    }    


    // Console branding
    console.log("Interactive visualization by Sam Hertig ––– www.samhertig.com");
});