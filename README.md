# Interactive Visualization of Research Areas

![previewgif](research-areas.gif)

A simple interactive javascript solution to visualize a department's research groups and their research areas on any website.

An example can be found [here](https://chab.ethz.ch/en/research/research-areas.html).


## Setup

### Incorporate the following snippet in your webpage and adapt the paths to the .js and .css files:

```
<link rel="stylesheet" type="text/css" href="viz.css">
<script type="text/javascript" src="d3.v4.min.js"></script>
<div id="viz"></div>
<script type="text/javascript" src="viz_DE.js"></script>
```
### Add your institution's data:

The research group and research area data is stored and maintained in a .csv file (see example files dataDE.csv and dataEN.csv). Adapt the path to the data file at the top of the viz_??.js-file if necessary.

Also the institutes, their URLs and there colors are defined in the viz_??.js file and have to be customized there.

Depending on your number of research groups, institutes and research areas you might also want to optimize other variables like the instituteLegendSpacer in the viz_??.js file.
